import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

declare var $: any;
import {HostListener} from '@angular/core';
import {TrackerReporterService} from './services/tracker-reporter.service';
import {HttpClient} from '../../node_modules/@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Find The Ducks';
  currentView = 0;

  userId;
  hitId;
  turkAss;

  currentPics = ['../assets/Images/1.jpg', '../assets/Images/2.jpg', '../assets/Images/3.jpg', '../assets/Images/4.jpg'];
  currentChangeIdx = 0;
  isStatisticsMode = true;
  timeLeft = 60 * 30;
  interval;

  disableNext = true;

  maxDucksToShow = 1;
  ducksApperaed = 0;
  ducksClicked = 0;
  timeOver = false;

  isEvilState = false;
  lastEvilApperaed = 60 * 10;
  wolvesAppered = 0;
  wolvesClicked = 0;

  isWolvesState = false;

  picsCilicked = [false, false, false, false];

  _q1Answer = '';
  _q2Answer = '';
  _q3Answer = '';
  q1Right = false;
  q2Right = false;
  q3Right = false;

  _r1Answer = '';
  _r2Answer = '';
  _r3Answer = '';
  _r4Answer = '';

  constructor(private activatedRoute: ActivatedRoute, private trackerReporter: TrackerReporterService, private http: HttpClient) {
    setInterval(() => {
      this.randomPics();
    }, 4000);
    // setInterval(() => {
    //   this.showWolves();
    // }, 1000 * 120);

    // setInterval(() => {
    //   this.reportToDB();
    // }, 5000);
    this.startTimer();
    this.http.get('https://api.ipify.org?format=json').subscribe(data => {
      this.trackerReporter.ipAddress = data['ip'];
    });
  }

  get r1Answer() {
    return this._r1Answer;
  }

  set r1Answer(currentAnswer) {
    this._r1Answer = currentAnswer;
  }

  get r2Answer() {
    return this._r2Answer;
  }

  set r2Answer(currentAnswer) {
    this._r2Answer = currentAnswer;
  }


  get r3Answer() {
    return this._r3Answer;
  }

  set r3Answer(currentAnswer) {
    this._r3Answer = currentAnswer;
  }

  get r4Answer() {
    return this._r4Answer;
  }

  set r4Answer(currentAnswer) {
    this._r4Answer = currentAnswer;
  }

  get q1Answer() {
    return this._q1Answer;
  }

  set q1Answer(currentAnswer) {
    if (currentAnswer === '10') {
      this.q1Right = true;
    } else {
      this.q1Right = false;
    }
    this._q1Answer = currentAnswer;
  }

  get q2Answer() {
    return this._q2Answer;
  }

  set q2Answer(currentAnswer) {
    if (currentAnswer === 'Ducks and Wolves') {
      this.q2Right = true;
    } else {
      this.q2Right = false;
    }
    this._q2Answer = currentAnswer;
  }

  get q3Answer() {
    return this._q3Answer;
  }

  set q3Answer(currentAnswer) {
    if (currentAnswer === 'Ducks') {
      this.q3Right = true;
    } else {
      this.q3Right = false;
    }
    this._q3Answer = currentAnswer;
  }

  ngOnInit() {
    // Note: Below 'queryParams' can be replaced with 'params' depending on your requirements
    // https://findtheduck-daeaf.firebaseapp.com/?workerId=A3JA8ROVK58W8U&hitId=qwwaszx&assignmentId=1q2a3z4x0
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.hasOwnProperty('assignmentId')) {
        // if (params['workerId'] !== 'A3JA8ROVK58W8U') {
        //   window.alert('We are looking for a specific worker..., Please Return the HIT!');
        //   this.disableNext = true;
        //   return;
        // }
        // TODO handle the case of unavailable assignment ID
        this.userId = params['workerId'];
        this.hitId = params['hitId'];
        this.turkAss = params['assignmentId'];
        if (this.turkAss === 'ASSIGNMENT_ID_NOT_AVAILABLE') {
          this.disableNext = true;
          return;
        }
        this.disableNext = false;
      } else {
        this.userId = 'blabla123';
        this.hitId = 'hit123';
        this.turkAss = 'ex2';
        this.disableNext = false;
      }
      this.trackerReporter.userId = this.userId;
      this.trackerReporter.hitId = this.hitId;
      this.trackerReporter.turkAss = this.turkAss;
      if (params.hasOwnProperty('assignmentId')) {
        this.trackerReporter.findUser({}).subscribe(res => {
            if (res.toString() !== '0') {
              window.alert('It seems its not the first time we meet... Please Return the HIT!');
              this.disableNext = true;
              return;
            } else {
              // this.trackerReporter.addNewUser({});
            }
          }, error1 => {
            this.disableNext = true;
            window.alert('Something went wrong... Please Return the HIT!');
          }
        );
      }
    });
  }

  goToDisagreePage() {
    this.currentView = -1;
  }

  continueToNextView() {
    if (this.currentView === 2) {
      if (this.q1Answer !== '10' || this.q2Answer !== 'Ducks and Wolves' || this.q3Answer !== 'Ducks') {
        window.alert('Wrong answers..');
        return;
      }
    }
    this.currentView += 1;
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.currentView === 3) {
        if (this.timeLeft > 0) {
          this.timeLeft--;
        } else {
          this.trackerReporter.addNewUser({});
          this.timeOver = true;
          this.trackerReporter.taskEnded = true;
        }
      }
    }, 1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

  changeMode() {
    this.isStatisticsMode = !this.isStatisticsMode;
  }

  showWolves() {
    const x = Math.floor(Math.random() * 30) + 1;
    setTimeout(() => {
      this.isWolvesState = true;
      this.wolvesAppered += 1;
      this.lastEvilApperaed = this.timeLeft;
      this.trackerReporter.numberOfWolvesAppeared += 1;
    }, x * 1000);
    // if (imgChosen === '../assets/Images/48.jpg' && this.picsCilicked[idx] === false) {
    //   this.wolvesClicked += 1;
    //   this.picsCilicked[idx] = true;
    //   this.trackerReporter.numberOfWolvesClicked += 1;
    //   this.isEvilState = false;
    // }
  }

  chooseImgWolf() {
    this.isWolvesState = false;
    this.wolvesClicked += 1;
    this.trackerReporter.numberOfWolvesClicked += 1;
    this.trackerReporter.lastImgClick = new Date();
    this.trackerReporter.numberOfClickOnImg += 1;
  }


  chooseImg(idx, imgChosen) {
    this.trackerReporter.lastImgClick = new Date();
    this.trackerReporter.numberOfClickOnImg += 1;
    // console.log(imgChosen);
    if (imgChosen === '../assets/Images/46.jpg' && this.picsCilicked[idx] === false) {
      this.ducksClicked += 1;
      this.picsCilicked[idx] = true;
      this.trackerReporter.numberOfDucksClicked += 1;
    }
    // console.log(this.ducksClicked);
    // console.log(this.trackerReporter.numberOfDucksClicked);
    //TODO: remove it when just ducks
    if (imgChosen === '../assets/Images/48.jpg' && this.picsCilicked[idx] === false) {
      this.wolvesClicked += 1;
      this.picsCilicked[idx] = true;
      this.trackerReporter.numberOfWolvesClicked += 1;
      this.isEvilState = false;
    }
    // console.log(this.wolvesClicked);
    // console.log(this.ducksClicked);
  }


  randomPics() {
    let randPicIdx;
    // for (i = 0; i < this.currentPics.length; i++) {
    //   randPicIdx = Math.floor(Math.random() * Math.floor(46)) + 1;
    //   if (randPicIdx === 46) {
    //     randPicIdx = 'duck';
    //   }
    //   this.currentPics[i] = '../assets/Images/' + randPicIdx.toString() + '.jpg';
    //   console.log(this.currentPics[i]);
    // }
    this.currentChangeIdx++;
    this.currentChangeIdx = this.currentChangeIdx % 4;
    const currentChangeIndex = this.currentChangeIdx;
    randPicIdx = this.randPicGenerate();
    this.picsCilicked[currentChangeIndex] = false;
    this.currentPics[currentChangeIndex] = '../assets/Images/' + randPicIdx.toString() + '.jpg';
    if (randPicIdx === 46 || randPicIdx === 48) {
      setTimeout(() => {
        console.log('duck is over');
        randPicIdx = this.randPicGenerate();
        this.picsCilicked[currentChangeIndex] = false;
        console.log(currentChangeIndex.toString() + '../assets/Images/' + randPicIdx.toString() + '.jpg');
        this.currentPics[currentChangeIndex] = '../assets/Images/' + randPicIdx.toString() + '.jpg';
      }, 4000);
    }
    console.log(this.ducksClicked);
  }

  randPicGenerate() {
    let randPicIdx = Math.floor(Math.random() * Math.floor(43)) + 1;
    // TODO: remove it when just 6/30 ducks
    if (this.timeLeft <= 60 * 17.5 && this.ducksApperaed === 0) {
      randPicIdx = 46;
      this.ducksApperaed += 1;
    }


    // TODO: 30 ducks
    // if (this.timeLeft <= 60 * 29 && this.ducksApperaed === 1) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 28 && this.ducksApperaed === 2) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 27 && this.ducksApperaed === 3) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 26.5 && this.ducksApperaed === 4) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 25.5 && this.ducksApperaed === 5) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 25 && this.ducksApperaed === 6) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 24 && this.ducksApperaed === 7) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 23 && this.ducksApperaed === 8) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 22 && this.ducksApperaed === 9) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 21.5 && this.ducksApperaed === 10) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 20 && this.ducksApperaed === 11) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 19 && this.ducksApperaed === 12) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 18 && this.ducksApperaed === 13) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 17 && this.ducksApperaed === 14) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 16.5 && this.ducksApperaed === 15) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 15 && this.ducksApperaed === 16) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 14 && this.ducksApperaed === 17) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 13 && this.ducksApperaed === 18) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 12 && this.ducksApperaed === 19) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 11 && this.ducksApperaed === 20) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 10 && this.ducksApperaed === 21) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 9.5 && this.ducksApperaed === 22) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 8 && this.ducksApperaed === 23) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 7 && this.ducksApperaed === 24) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 6.5 && this.ducksApperaed === 25) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 5 && this.ducksApperaed === 26) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 4 && this.ducksApperaed === 27) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 3 && this.ducksApperaed === 28) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 2 && this.ducksApperaed === 29) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }

    // TODO: 6 ducks
    // if (this.timeLeft <= 60 * 27 && this.ducksApperaed === 0) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 22.5 && this.ducksApperaed === 1) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 19 && this.ducksApperaed === 2) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 12 && this.ducksApperaed === 3) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 9 && this.ducksApperaed === 4) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 6 && this.ducksApperaed === 5) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }


    // TODO: wolves
    // if (this.timeLeft <= 60 * 27 && this.wolvesAppered === 0) {
    //   randPicIdx = 48;
    //   this.wolvesAppered += 1;
    // }
    // if (this.timeLeft <= 60 * 22.5 && this.wolvesAppered === 1) {
    //   randPicIdx = 48;
    //   this.wolvesAppered += 1;
    // }
    // if (this.timeLeft <= 60 * 19 && this.wolvesAppered === 2) {
    //   randPicIdx = 48;
    //   this.wolvesAppered += 1;
    // }
    // if (this.timeLeft <= 60 * 12 && this.ducksApperaed === 0) {
    //   randPicIdx = 46;
    //   this.ducksApperaed += 1;
    // }
    // if (this.timeLeft <= 60 * 9 && this.wolvesAppered === 3) {
    //   randPicIdx = 48;
    //   this.wolvesAppered += 1;
    // }
    // if (this.timeLeft <= 60 * 6 && this.wolvesAppered === 4) {
    //   randPicIdx = 48;
    //   this.wolvesAppered += 1;
    // }

    if (randPicIdx === 46) {
      this.trackerReporter.numberOfDucksAppeared += 1;
    }
    if (randPicIdx === 48) {
      this.trackerReporter.numberOfWolvesAppeared += 1;
    }

    return randPicIdx;
  }

  // finishHit() {
  //   // TODO remove that condition
  //     console.log('wowwww');
  //     this.http.post('https://www.mturk.com/mturk/externalSubmit', {
  //       // 'workerId': this.userId,
  //       // 'hitId': this.hitId,
  //       'assignmentId': this.turkAss
  //     }).subscribe(res => {
  //       window.location.assign('https://www.mturk.com/mturk/externalSubmit');
  //     });
  // }

}
