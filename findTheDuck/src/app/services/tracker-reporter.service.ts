import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TrackerReporterService {

  // TODO: add your server side details to store data:
  trackerUrl = '';
  userCreationUrl = '';
  userSearchUrl = '';
  userId;
  hitId;
  turkAss;
  lastImgClick = null;
  numberOfClickOnImg = 0;
  numberOfDucksClicked = 0;
  numberOfDucksAppeared = 0;
  numberOfWolvesClicked = 0;
  numberOfWolvesAppeared = 0;
  ipAddress = '';
  taskEnded = false;


  constructor(private http: HttpClient) {
  }

  addTrackRecord(data: {}) {
    data['userId'] = this.userId;
    data['hitId'] = this.hitId;
    data['turkAss'] = this.turkAss;
    if (!this.taskEnded) {
      return this.http.post(this.trackerUrl, data);
    }
  }

  addNewUser(data: {}) {
    data['userId'] = this.userId;
    return this.http.post(this.userCreationUrl, data).subscribe(res => {
      }
    );
  }

  findUser(data: {}) {
    data['userId'] = this.userId;
    // console.log(data);
    return this.http.post(this.userSearchUrl, data);
  }

}
