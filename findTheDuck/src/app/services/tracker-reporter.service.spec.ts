import { TestBed, inject } from '@angular/core/testing';

import { TrackerReporterService } from './tracker-reporter.service';

describe('TrackerReporterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrackerReporterService]
    });
  });

  it('should be created', inject([TrackerReporterService], (service: TrackerReporterService) => {
    expect(service).toBeTruthy();
  }));
});
