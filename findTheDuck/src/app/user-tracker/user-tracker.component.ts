import {Component, OnInit} from '@angular/core';

declare var $: any;
import {HostListener} from '@angular/core';
import {TrackerReporterService} from '../services/tracker-reporter.service';
import {
  OnPageVisible, OnPageHidden,
  OnPageVisibilityChange,
  AngularPageVisibilityStateEnum,
  OnPagePrerender, OnPageUnloaded
} from 'angular-page-visibility';


@Component({
  selector: 'app-user-tracker',
  templateUrl: './user-tracker.component.html',
  styleUrls: ['./user-tracker.component.css']
})
export class UserTrackerComponent implements OnInit {

  mouseX;
  mouseY;
  cursorInPage = 'Yes';
  lastCursorInPageChange = new Date();
  lastTabFocus = new Date();
  tabFocused = 'Tab is Focused';

  lastMouseMove = new Date();
  lastImgClick = null;
  numberOfClickOnImg = 0;
  numberOfDucksClicked = 0;
  numberOfDucksAppeared = 0;
  numberOfWolvesClicked = 0;
  numberOfWolvesAppeared = 0;

  innerWidth;
  innerHeight;
  screenWidth;
  screenHeight;

  timezoneData;
  platformData;
  langsData;
  currentDate = new Date();
  remainedTime = 30 * 60 + 1;

  constructor(private trackerReporter: TrackerReporterService) {
    this.screenWidth = screen.width;
    this.screenHeight = screen.height;
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const d = new Date();
    const tz = d.toString().split('GMT')[1].split(' (')[0];
    this.timezoneData = timezone + ' --- ' + tz;
    this.platformData = navigator.userAgent;
    this.langsData = navigator.languages + ' The chosen is: ' + navigator.language;
    setInterval(() => {
      this.updateTimePassed();
    }, 200);
    setInterval(() => {
      this.trackChanges();
    }, 200);
    setInterval(() => {
      this.reportToDB();
    }, 1000);
  }

  ngOnInit() {
  }

  reportToDB() {
    this.currentDate = new Date();
    this.lastImgClick = this.trackerReporter.lastImgClick;
    this.numberOfClickOnImg = this.trackerReporter.numberOfClickOnImg;
    this.numberOfDucksClicked = this.trackerReporter.numberOfDucksClicked;
    this.numberOfDucksAppeared = this.trackerReporter.numberOfDucksAppeared;
    this.numberOfWolvesClicked = this.trackerReporter.numberOfWolvesClicked;
    this.numberOfWolvesAppeared = this.trackerReporter.numberOfWolvesAppeared;
    this.remainedTime = this.remainedTime - 1;
    if (this.trackerReporter.taskEnded) {
      return;
    }
    this.trackerReporter.addTrackRecord({
      'mouseX': this.mouseX,
      'mouseY': this.mouseY,
      'lastImgClick': this.lastImgClick,
      'numberOfClickImg': this.numberOfClickOnImg,
      'numberOfDucksClicked': this.numberOfDucksClicked,
      'numberOfDucksAppeared': this.numberOfDucksAppeared,
      'numberOfWolvesClicked': this.numberOfWolvesClicked,
      'numberOfWolvesAppeared': this.numberOfWolvesAppeared,
      'cursorInPage': this.cursorInPage,
      'tabFocused': this.tabFocused,
      'lastMouseMove': this.lastMouseMove,
      'innerWidth': this.innerWidth,
      'innerHeight': this.innerHeight,
      'screenWidth': this.screenWidth,
      'screenHeight': this.screenHeight,
      'langData': this.langsData,
      'osData': this.platformData,
      'timezoneData': this.timezoneData,
      'ip': this.trackerReporter.ipAddress,
      'timeLeft': this.remainedTime,
      'currentDateClient': this.currentDate,
      'secondsWithoutMove': (this.currentDate.getTime() - this.lastMouseMove.getTime()) / 1000,
      'secondsCursorPage': (this.currentDate.getTime() - this.lastCursorInPageChange.getTime()) / 1000,
      'secondsTabFocus': (this.currentDate.getTime() - this.lastTabFocus.getTime()) / 1000,
      'label': '15'
    }).subscribe(res => {
      // console.log(res);
    });
  }

  updateTimePassed() {
    this.currentDate = new Date();
  }

  trackChanges() {
    if (document.hasFocus()) {
      this.tabFocused = 'Tab is Focused';
    } else {
      this.tabFocused = 'Tab is out of Focus';
    }
    this.lastTabFocus = new Date();
  }


  @OnPageVisible()
  logWhenPageVisible(): void {
    // console.log( 'OnPageVisible' );
    // console.log( 'visible' );
  }

  @OnPageHidden()
  logWhenPageHidden(): void {
    // console.log( 'OnPageHidden' );
    // console.log( 'hidden' );
  }

  @OnPageVisibilityChange()
  logWhenPageVisibilityChange(isPageVisible: boolean): void {
    // console.log( 'OnPageVisibilityChange' );
    // if ( isPageVisible ) {
    //   console.log( 'visible' );
    // } else {
    //   console.log( 'hidden' );
    // }
  }

  @HostListener('document:mouseout', ['$event'])
  onMouseOut(e) {
    this.cursorInPage = 'No';
    this.lastCursorInPageChange = new Date();
  }


  @HostListener('document:mouseover', ['$event'])
  onMouseOver(e) {
    this.cursorInPage = 'Yes';
    this.lastCursorInPageChange = new Date();
    // console.log(this.lastCursorInPageChange);
  }


  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    this.mouseX = e.pageX;
    this.mouseY = e.pageY;
    this.lastMouseMove = new Date();
  }

  @HostListener('window:onfocus', ['$event'])
  onFocus(event) {
    // console.log('focuss');
    this.tabFocused = 'Tab is Focused';
    this.lastTabFocus = new Date();
  }


  // @HostListener('window:onblur', ['$event'])
  // onBlur(event) {
  //   console.log('blurrr');
  //   this.tabFocused = 'Tab is out of Focus';
  // }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }

}
